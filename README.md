# README
# ANOTHERONE
```
apiary url: http://docs.conferencewall.apiary.io/
Steps to migrate from sqlite to Postgres:
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install postgresql postgresql-contrib libpq-dev
sudo gem install pg
-> Modify gem 'sqlite3' to: gem 'pg'
bundle install
sudo -s
su - postgres
psql
CREATE DATABASE conferencewall_dev;
CREATE DATABASE conferencewall_tst;
ALTER ROLE admin SUPERUSER;
create role admin with createdb login password 'admin';
vi /etc/postgresql/9.5/main/pg_hba.conf
-> Modify    local all all peer    to    local all all md5
service postgresql stop
service postgresql start
-> Modify config/database.yml
  ```
  ```
development:
  adapter: postgresql
  encoding: unicode
  database: conferencewall_dev
  pool: 5
  timeout: 5000
  username: admin
  password: admin
test:
  adapter: postgresql
  encoding: unicode
  database: conferencewall_tst
  pool: 5
  timeout: 5000
  username: admin
  password: admin
production:
  adapter: postgresql
  encoding: utf8
  database: conferencewall_prd
  pool: 5
  timeout: 5000
  username: admin
  password: admin
  ```
  ```
rake db:migrate
bin/rails db:migrate RAILS_ENV=test
```
