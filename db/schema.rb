# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170321190316) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "users", force: :cascade do |t|
    t.string   "provider",            null: false
    t.string   "authorization_token"
    t.string   "name"
    t.string   "access_token",        null: false
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "access_token_secret"
    t.index ["authorization_token"], name: "index_users_on_authorization_token", using: :btree
    t.index ["provider", "authorization_token"], name: "index_users_on_provider_and_authorization_token", unique: true, using: :btree
    t.index ["provider"], name: "index_users_on_provider", using: :btree
  end

  create_table "walls", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string   "name",                    null: false
    t.text     "tags",       default: [], null: false, array: true
    t.integer  "user_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["user_id"], name: "index_walls_on_user_id", using: :btree
  end

  add_foreign_key "walls", "users"
end
