class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :provider, null: false
      t.string :authorization_token, null: true
      t.string :name
      t.string :access_token, null: false

      t.timestamps
    end
    add_index :users, :provider
    add_index :users, :authorization_token
    add_index :users, [:provider, :authorization_token], unique: true
  end
end
