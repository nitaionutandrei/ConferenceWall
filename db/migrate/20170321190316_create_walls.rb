class CreateWalls < ActiveRecord::Migration[5.0]
  def change
    create_table :walls, id: :uuid do |t|
      t.string :name, null: false
      t.text :tags, array: true, null: false, default: []
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
