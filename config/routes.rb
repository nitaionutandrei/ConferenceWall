require 'sidekiq/web'
Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      post 'registrations/auth'
      resource :tweets
    end
  end
  get '/auth/:provider/callback', to: 'auth#callback'
  resource :auth, only: :create
  resource :pages, only: :show
  get '/walls/', to: 'walls#index'
  get '/walls/:id', to: 'walls#show'
  post '/walls', to: 'walls#create'
  delete '/walls/:id', to: 'walls#delete'
  patch '/walls/:id', to: 'walls#update'
  root to: 'pages#welcome'
  mount ActionCable.server => '/cable'
  mount Sidekiq::Web => '/sidekiq'
end
