set :stage, :staging
set :nginx_server_name, "api.staging.conferencewall.stream"
server "api.staging.conferencewall.stream", user: fetch(:user) , roles: %w{web app db}, primary: true
