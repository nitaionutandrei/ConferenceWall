class User < ApplicationRecord
  has_many :walls

  class << self
    def from_omniauth(provider, access_token, access_token_secret)
      user = find_or_create_by(provider: provider, access_token: access_token)
      unless user.authorization_token?
        user.authorization_token = SecureRandom.uuid
        client = twitter_client(access_token, access_token_secret)
        user.name = client.user.name
        user.access_token_secret = access_token_secret
        user.save
      end
      user
    end
    private
    def twitter_client(access_token, access_token_secret)
      Twitter::REST::Client.new do |config|
        config.consumer_key        = Rails.application.secrets.twitter_key
        config.consumer_secret     = Rails.application.secrets.twitter_secret
        config.access_token        = access_token
        config.access_token_secret = access_token_secret
      end
    end
  end
end
