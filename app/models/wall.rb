class Wall < ApplicationRecord
  #TODO validations
  belongs_to :user
  validates :name, uniqueness: { scope: :user_id }
end
