require 'tweetstream'
require 'twitter'
require 'twitter/tweet'
require 'http'
require 'buftok'
require 'http'
require 'json'

class TweetWorker
  def run
    # start streaming public tweets
    begin
      puts 'Tweet worker running...'

      redis = Redis.new
      @newFilters = int_to_bool redis.getbit('newTags', 7)
      process_id = nil
      while true
        if @newFilters
          redis.setbit('newTags', 7, 0)
          @newFilters = int_to_bool redis.getbit('newTags', 7)
          Process.kill('SIGKILL',p1) if(process_id)
          sleep 3
          p1 = fork &method(:restartStream)
        else
          @newFilters = int_to_bool redis.getbit('newTags', 7)
          sleep 1
        end
      end
    rescue => ex
      puts ex.message
      puts ex.backtrace.join("\n")
    end
  end

  def restartStream
    redis = Redis.new
    filters = redis.smembers("watched_tags")
    puts "started streaming for filters: #{filters}"
    client = Twitter::Streaming::Client.new do |config|
      config.consumer_key        = Rails.application.secrets.twitter_key
      config.consumer_secret     = Rails.application.secrets.twitter_secret
      config.access_token        = '806931617864257536-TOpyb7h3oYKNWkRQVcAY4ty6EED1PT2'
      config.access_token_secret = 'RKsHtZVp6pQZqoPnG2hqrDspF3searYXquGE9VL4ySTlt'
    end
    client.filter(track: filters.join(',')) do |object|
      if !@newFilters && object.is_a?(Twitter::Tweet)
        TweetDealerJob.perform_later(object.text, object.user.name, object.user.profile_image_url.to_s)
      end
    end
    puts 'stream stopped'
  end

  def self.run
    new.run
  end

  # def redis
  #     redis = Redis.current
  # end
end

def int_to_bool(int)
  int.to_i > 0
end

class Integer
  def try_to_b
    !self.zero?
  end
end
