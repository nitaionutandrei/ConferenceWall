class ApplicationController < ActionController::Base
  rescue_from ActiveRecord::RecordNotFound, with: :not_found
  protect_from_forgery with: :null_session
  protected
    def current_user
      @current_user ||= User.find_by(authorization_token: request.headers[:Authorization])
    end

    def logged_in?
      !!current_user
    end

    def authenticate_user!
      unless logged_in?
        self.headers['WWW-Authenticate'] = 'Token realm="Application"'
        render json: { errors: 'Bad credentials' }, status: 401
      end
    end

    def not_found(e)
        render json: {message: 'record not found'}, status: 404
    end
end
