class Api::V1::RegistrationsController < ApplicationController
  before_action :set_default_response_format
  def auth
    @user = User.from_omniauth(params[:provider], params[:access_token], params[:access_token_secret])
  end
  
  private
    def set_default_response_format
      request.format = :json
    end
end
