class Api::V1::TweetsController < ApplicationController
  def show
    # get access_token and access_token_secret of the user
    user = User.find_by authorization_token: params[:authorization_token]
    # make streaming client instance
    client = Twitter::Streaming::Client.new do |config|
      config.consumer_key        = ENV['TWITTER_KEY']
      config.consumer_secret     = ENV['TWITTER_SECRET']
      config.access_token        = user.access_token
      config.access_token_secret = user.access_token_secret
    end

    ActionCable.server.broadcast "tweets:#{Wall.first.id}", 'test123'
    topics = ['germany']
    client.filter(track: topics.join(',')) do |object|
      if object.is_a?(Twitter::Tweet)
        puts object.text
        #TweetsChannel.broadcast_to(
        #  'tweets',
        #  tweet: object.text,
        #  image: 'image_url_here'
        #)
        ActionCable.server.broadcast "tweets:#{Wall.first.id}", object.text
      end
    end
  end
end
