class WallsController < ApplicationController
  before_action :set_default_response_format
  before_action :authenticate_user!, except: :show
  before_action :find_wall, only: [:show, :update, :delete]

  def index
    @walls = @current_user.walls
    render json: @walls.to_json(only: [:id, :name, :tags]), status: 200
  end

  def show
    if @wall
      render json: @wall.to_json(only: [:id, :name, :tags]), status: 200
    else
      render json: { errors: 'Wall cannot be found.' }, status: 422
    end
  end

  def create
    @wall = current_user.walls.new(permitted_params)
    if @wall.save
      render json: @wall.to_json(only: [:id, :name, :tags]), status: 201
    else
      render json: @wall.errors, status: 422
    end
  end

  def update
    if @wall.update(permitted_params)
      render json: @wall.to_json(only: [:id, :name, :tags]), status: 200
    else
      render json: @wall.errors, status: 422
    end
  end

  def delete
    if @wall
      @wall.destroy
    else
      render json: { errors: 'Wall cannot be found.' }, status: 422
    end
  end

  private
    def set_default_response_format
      request.format = :json
    end

    def find_wall
      @wall = Wall.find(params[:id])
    end

    def permitted_params
      params.permit(:name, tags: [])
    end
end
