class AuthController < ApplicationController
  def callback
    user = User.from_omniauth(params[:provider], auth_hash.token, auth_hash.secret)
    cookies[:authorization_token] = user.authorization_token
    redirect_to pages_path
  end
  
  def create
  end
  private
    def auth_hash
      request.env['omniauth.auth'].credentials
    end
end
