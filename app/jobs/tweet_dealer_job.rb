class TweetDealerJob < ApplicationJob
  queue_as :default

  def perform(text, user, image_url)

    htags = text.scan(/(#\w+)/)

    #puts "#{args}"
    puts "found tags:#{htags}"
    htags.each do |tag|
      # binding.pry
      Redis.current.smembers("#{tag[0].downcase}").each do |wall_id|
        ActionCable.server.broadcast "tweets:#{wall_id}", {text: text,
          user: user, image_url: image_url}
        puts "#{Time.now}: Broadast to -> #{wall_id}"
      end
    end
  end
end
