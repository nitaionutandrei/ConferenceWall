# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
class TweetsChannel < ApplicationCable::Channel
require "redis"

  def subscribed
    stream_from "tweets:#{current_wall.id}"
    # tsw = TweetsStreamerWorker.new
    redis = Redis.new
    tagsBefore = redis.scard('watched_tags')
    current_wall.tags.each do |htag|
        redis.sadd(htag, current_wall.id)
        redis.sadd("watched_tags", htag)
    end
    tagsAfter = redis.scard('watched_tags')
    if tagsBefore != tagsAfter
      redis.setbit('newTags', 7, 1)
    end
    redis.incr("wall_usage_count:#{current_wall.id}")
  end

  def unsubscribed
    redis = Redis.new
    redis.decr("wall_usage_count:#{current_wall.id}")
    remaining_usages = redis.get("wall_usage_count:#{current_wall.id}").to_i
    unless remaining_usages > 0
      current_wall.tags.each do |htag|
          redis.srem(htag, current_wall.id)
          redis.srem("watched_tags", htag)

          # redis.setbit('newTags', 7, 1)
          # posibil sa fie in alte conexiuni
      end
      if redis.smembers("watched_tags").nil?
        # set worker idle
      end
    end
    #TODO if subscribers for this channel == 0 end straming
  end

  def retrieve

  end
end
