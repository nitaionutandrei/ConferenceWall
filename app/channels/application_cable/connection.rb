module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_wall
    def connect
      self.current_wall = find_verified_wall
    end

    private
      def find_verified_wall
        if current_wall = Wall.find_by(id: request.params[:wall_id])
          current_wall
        else
          reject_unauthorized_connection
        end
      end
  end
end
