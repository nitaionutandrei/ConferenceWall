require 'rails_helper'

RSpec.describe 'Registrations', type: :request do
  describe 'POST /registrations' do
    it 'sends parameters to User.from_omniauth' do
      user = double
      allow(user).to receive(:name).and_return('Petrica')
      client = double
      allow(client).to receive(:user).and_return(user)
      allow(User).to receive(:twitter_client).with('access_token', 'secret').and_return(client)
      post '/api/v1/registrations/auth', params: { provider: 'twitter',
                                                    access_token: 'access_token',
                                                    access_token_secret: 'secret' }
      resp_body = json(response.body)
      expect(resp_body['name']).to eq('Petrica')
      expect(resp_body['authorization_token']).to be_present
    end
  end
end
