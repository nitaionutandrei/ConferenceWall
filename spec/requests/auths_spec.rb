require 'rails_helper'

RSpec.describe 'Auths', type: :request do
  describe 'Login with twitter' do
    it 'responds with something' do
      user = double
      allow(user).to receive(:name).and_return('Petrica')
      client = double
      allow(client).to receive(:user).and_return(user)
      allow(User).to receive(:twitter_client).with('access_token', 'secret').and_return(client)
      cookies[:name] = user.name
      cookies[:authorization_token] = '123-456'
      get '/auth/twitter/callback'
      expect(response).to have_http_status(302)
      expect(cookies['authorization_token']).to be_present
      expect(cookies['name']).to be_present
    end
  end
end
